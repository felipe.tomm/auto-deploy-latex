Auto Deploy LaTeX
=================

This repo was created to document how to automatically deploy a PDF document compiled from a TeX file using GitLab's Continuous Integration and Pages.

It also provides an example of a simple document that can be viewed `here`_.

For an easy start by forking this repo and then editing the main.tex file in latex\\. This example uses latexmk and lualatex to compile the PDF. If you need to edit the script used change the .gitlab-ci.yml file.

For more information please read below.

Target Link
-----------

The document will be published as a GitLab page in the following link: :code:`https://<USERNAME>.gitlab.io/<REPOSITORY>/<DOCUMENT>`.

To access the document of this example go to: :code:`https://bgameiro.gitlab.io/auto-deploy-latex/document.pdf`.

Privacy and Repo configuration
------------------------------

Gitlab allows the repo to be set to private and still publish the document as a visible page.

This is a great feature since it allws you to keep your TeX files private while keeping the PDF public.

Compiling script
----------------

Frequency of deployment
-----------------------

Branches
--------

In this example, only the files committed to the master branch will be compiled.

This allows for you to keep other branches where you can test the document output without overwriting the last desirable document.

Git Ignore
----------

------------

For more information refer to my `post`_ where I present the motivation for this simple, yet useful, project.

.. _here: https://bgameiro.gitlab.io/auto-deploy-latex/document.pdf

.. _post: https://www.linkedin.com/in/bgameiro